from dataclasses import dataclass
from typing import *
import math
from functools import total_ordering

class Expr:
    def evaluate(self, context):
        raise NotImplementedError("This reeeealy shouldn't happen")

class Atom(Expr):
    def evaluate(self, context):
        return self

@dataclass
class Int(Atom):
    value: int

    @staticmethod
    def convertFrom(x):
        if type(x) == Int:
            return x
    def __truediv__(self, other):
        return Fraction.create(self.value, other.value)

@dataclass
@total_ordering
class Fraction(Atom):
    a: int
    b: int
    def create(a, b):
        def D(a,b):
            if b==0:
                return a
            return D(b, a%b)
        if b==0:
            raise ZeroDivisionError()
        sign = 1 if (a<=0 and b<=0) or (a>=0 and b>=0) else -1
        a,b = abs(a), abs(b)
        d = D(max(a,b), min(a,b))
        ans = Fraction(sign * a//d, b//d)
        if max(ans.a, ans.b) > 1e20:
            return Float.convertFrom(ans)
        return ans

    @property
    def value(self):
        return self.a/self.b

    @staticmethod
    def convertFrom(x):
        if type(x) == Int:
            return Fraction(x.value, 1)
        elif type(x) == Fraction:
            return x
    def __add__(self, other):
        return Fraction.create(self.a*other.b + self.b*other.a, self.b*other.b)
    def __sub__(self, other):
        return Fraction.create(self.a*other.b - self.b*other.a, self.b*other.b)
    def __mul__(self, other):
        return Fraction.create(self.a*other.a, self.b*other.b)
    def __truediv__(self, other):
        return Fraction.create(self.a*other.b, self.b*other.a)
    def __mod__(self, other):
        u,v = self.a*other.b, self.b*other.a
        return Fraction.create(u - u//v*v, self.b*other.b)
    def __pow__(self, other):
        return Complex((self.a / self.b) ** (other.a / other.b))
    def __eq__(self, other):
        return self.a * other.b == self.b * other.a
    def __ne__(self, other):
        return self.a * other.b != self.b * other.a
    def __ge__(self, other):
        return self.a * other.b >= self.b * other.a

@dataclass
class Float(Atom):
    value: float

    @staticmethod
    def convertFrom(x):
        if type(x) == Int:
            return Float(x.value)
        elif type(x) == Fraction:
            return Float(x.a / x.b)
        elif type(x) == Float:
            return x
    def __pow__(self, other):
        return Complex(self.value ** other.value)
    def __getattr__(self, item):
        return getattr(self.value, item)

@dataclass
class Complex(Atom):
    value: complex

    @staticmethod
    def convertFrom(x):
        if type(x) == Int:
            return Complex(x.value)
        elif type(x) == Fraction:
            return Complex(x.a / x.b)
        elif type(x) == Float:
            return Complex(x.value)
        elif type(x) == Complex:
            return x
    def __getattr__(self, item):
        return getattr(self.value, item)

@dataclass
class Bool(Atom):
    value: bool

@dataclass
class Vector(Expr):
    elements: List[Expr]
    def evaluate(self, context):
        return Vector([i.evaluate(context) for i in self.elements])

@dataclass
class Variable(Expr):
    name: str
    def evaluate(self, context):
        try:
            return context[self.name].evaluate(context)
        except KeyError:
            raise NameError(f"Variable {self.name} is not defined")

@dataclass
class Function(Atom):
    args: List[Variable]
    body: Expr
    def evaluate(self, context):
        if self not in (_if, _and, _or):
            return Closure(self.args, self.body, context.copy())
        return self

@dataclass
class Call(Expr):
    name: Expr
    args: List[Expr]
    def evaluate(self, context):
        self.name = f = self.name.evaluate(context)
        lazy = self.name in (_if, _and, _or)
        if not isinstance(f, Function):
            raise TypeError("Tried to call non-function")
        if len(f.args) != len(self.args):
            raise Exception(f"Wrong number of arguments {len(self.args)}, excepted {len(f.args)}")
        context.enter()
        for var,val in zip(f.args, self.args):
            context[var.name] = val if lazy else val.evaluate(context)
        if isinstance(f, Closure) and context is not f.context:
            for i in context.stack:
                f.context.stack.append(i)
            ans = f.body.evaluate(f.context)
            for i in context.stack:
                f.context.stack.pop()
        else:
            ans = f.body.evaluate(context)
        context.leave()
        return ans

class Context:
    def __init__(self):
        self.stack = []
    def enter(self):
        self.stack.append({})
    def leave(self):
        self.stack.pop()
    def __setitem__(self, key, value):
        self.stack[-1][key] = value
    def __getitem__(self, item):
        for i in range(len(self.stack)-1, -1, -1):
            try:
                return self.stack[i][item]
            except KeyError:
                pass
        raise KeyError(item)
    def copy(self):
        c = Context()
        for i in self.stack:
            c.stack.append(i.copy())
            pass
        return c

@dataclass
class Closure(Function):
    args: List[Variable]
    body: Expr
    context: Context
    def evaluate(self, context):
        return self

def defineFunction(argc, function):
    argnames = [f"${i}" for i in range(argc)]
    args = [Variable(argname) for argname in argnames]
    class MyExpr(Expr):
        def evaluate(self, context):
            return function(context, *args)
    return Function(args=args, body=MyExpr())

def arithmetic(op, resultType=None):
    def f(context, a, b):
        a,b = a.evaluate(context), b.evaluate(context)
        order = [Int, Fraction, Float, Complex]
        order2 = {j:i for i,j in enumerate(order)}
        try:
            newtype = order[max(order2[type(a)], order2[type(b)])]
        except KeyError:
            raise Exception("Arithmetic operation on wrong types")
        a,b = newtype.convertFrom(a), newtype.convertFrom(b)
        try:
            return op(a,b)
        except TypeError:
            return (resultType if resultType else newtype)(op(a.value, b.value))
    return f

def integral(op):
    def f(context, a, b):
        a, b = a.evaluate(context), b.evaluate(context)
        a, b = Complex.convertFrom(a), Complex.convertFrom(b)
        if not (a.real == int(a.real) and a.imag == 0 and b.real == int(b.real) and b.imag == 0):
            raise Exception("Arithmetic operation on wrong types")
        a, b = int(a.real), int(b.real)
        return Int(op(a,b))
    return f

def isReal(val):
    return type(val) in (Int, Fraction, Float) or (isinstance(val, Complex) and val.value.imag == 0)

def realize(val):
    return Float(val.value.real) if isinstance(val, Complex) else val

def _if(context, p, a, b):
    p = p.evaluate(context)
    if not isinstance(p, Bool):
        raise Exception("First parameter of if is not a condition")
    if p.value:
        return a.evaluate(context)
    return b.evaluate(context)
def _and(context, a, b):
    a = a.evaluate(context)
    if not isinstance(a, Bool):
        raise Exception("Logical operation on wrong types")
    if not a.value:
        return Bool(False)
    b = b.evaluate(context)
    if not isinstance(b, Bool):
        raise Exception("Logical operation on wrong types")
    return b
def _or(context, a, b):
    a = a.evaluate(context)
    if not isinstance(a, Bool):
        raise Exception("Logical operation on wrong types")
    if a.value:
        return Bool(True)
    b = b.evaluate(context)
    if not isinstance(b, Bool):
        raise Exception("Logical operation on wrong types")
    return b

_and, _or, _if = defineFunction(2, _and), defineFunction(2, _or), defineFunction(3, _if)

def createStdContext():
    context = Context()
    context.enter()
    def _not(context, a):
        a = a.evaluate(context)
        if not isinstance(a, Bool):
            raise Exception("Logical operation on wrong types")
        return Bool(not a.value)
    def sum(context, od, po, f):
        od, po, f = od.evaluate(context), po.evaluate(context), f.evaluate(context)
        if not isReal(od) or not isReal(po):
            raise Exception("First 2 parameters of sum must be reals")
        od, po = realize(od), realize(po)
        ans = Int(0)
        plus = arithmetic(lambda x, y: x + y)
        cmp = arithmetic((lambda x, y: x <= y), Bool)
        while cmp(context, od, po).value:
            ans = plus(context, ans, Call(f, [od]).evaluate(context))
            od = plus(context, od, Int(1))
        return ans
    def product(context, od, po, f):
        od, po, f = od.evaluate(context), po.evaluate(context), f.evaluate(context)
        if not isReal(od) or not isReal(po):
            raise Exception("First 2 parameters of product must be reals")
        od, po = realize(od), realize(po)
        ans = Int(1)
        mul = arithmetic(lambda x,y: x*y)
        plus = arithmetic(lambda x, y: x + y)
        cmp = arithmetic((lambda x, y: x <= y), Bool)
        while cmp(context, od, po).value:
            ans = mul(context, ans, Call(f, [od]).evaluate(context))
            od = plus(context, od, Int(1))
        return ans
    def root(c, x, n):
        x,n = x.evaluate(c), n.evaluate(c)
        m = realize(n).value
        if not isReal(n) or m != int(m) or m <= 0:
            raise Exception("Second parameter of root has to be positive integer")
        if not isReal(x):
            raise Exception(f"Trying to compute {n}-th root of complex number. Use ^(1/{n}) instead.")
        x = realize(x).value
        if x<0:
            if m%2==0:
                raise Exception("Even root of negative number")
            return Float(-(-x)**(1/m))
        return Float(x**(1/m))
    for f in (
        "acos", "acosh", "asin", "asinh", "atan", "atanh", "ceil", "cos", "cosh",
        "degrees", "erf", "erfc", "exp" "floor", "gamma",
        "lgamma", "radians", "sin", "sinh", "tan", "tanh"
    ):
        context[f] = defineFunction(1, (lambda f : lambda c, x: (Float(getattr(math, f)(x.evaluate(c).value))))(f))

    context["gcd"] = defineFunction(2, integral(math.gcd))
    context["lcm"] = defineFunction(2, integral(lambda a,b : 0 if a==b==0 else a*b//math.gcd(a,b)))
    context["norm"] = defineFunction(2, arithmetic(math.hypot))
    context["ln"] = defineFunction(1, lambda c,x : Float(math.log(x.evaluate(c).value)))
    context["log"] = defineFunction(2, arithmetic(lambda a,b : math.log(b,a)))
    context["real"] = defineFunction(1, lambda c,x : Float(Complex.convertFrom(x.evaluate(c)).real))
    context["imag"] = defineFunction(1, lambda c,x : Float(Complex.convertFrom(x.evaluate(c)).imag))
    context["int"] = context["floor"] = defineFunction(1, lambda c,x : Int(int(Complex.convertFrom(x.evaluate(c)).real)))
    context["+"] = defineFunction(2, arithmetic(lambda x,y: x+y))
    context["-"] = defineFunction(2, arithmetic(lambda x,y: x-y))
    context["*"] = defineFunction(2, arithmetic(lambda x,y: x*y))
    context["/"] = defineFunction(2, arithmetic(lambda x,y: x/y))
    context["^"] = defineFunction(2, arithmetic(lambda x,y: x**y))
    context["%"] = defineFunction(2, arithmetic(lambda x,y: x%y))
    context["√"] = context["sqrt"] = defineFunction(1, lambda c, x: root(c, x, Int(2)))
    context["∛"] = context["cbrt"] = defineFunction(1, lambda c, x: root(c, x, Int(3)))
    context["∜"] = context["tsrt"] = defineFunction(1, lambda c, x: root(c, x, Int(4)))
    context["root"] = defineFunction(2, root)
    context["!"] = defineFunction(1, lambda c, x: Int(math.factorial(x.evaluate(c).value)))
    def cust_abs(c, x):
        evaluated = x.evaluate(c)
        return type(evaluated)(abs(evaluated.value))
    context["abs"] = defineFunction(1, cust_abs)

    context["="] = defineFunction(2, arithmetic((lambda x, y: x<=y and x>=y), Bool))
    context["<"] = defineFunction(2, arithmetic((lambda x, y: x < y), Bool))
    context[">"] = defineFunction(2, arithmetic((lambda x, y: x > y), Bool))
    context["<="] = context["≤"] = defineFunction(2, arithmetic((lambda x, y: x <= y), Bool))
    context[">="] = context["≥"] = defineFunction(2, arithmetic((lambda x, y: x >= y), Bool))
    context["/="] = context["≠"] = defineFunction(2, arithmetic((lambda x, y: x<y or x>y), Bool))

    context["$and"] = _and
    context["$or"] = _or
    context["$not"] = defineFunction(1, _not)

    context["$if"] = _if

    context["true"] = Bool(True)
    context["false"] = Bool(False)
    context["i"] = Complex(complex(0, 1))
    context["π"] = context["pi"] = Float(math.pi)
    context["τ"] = context["tau"] = Float(2*math.pi)
    context["e"] = Float(math.e)

    context["⅀"] = context["sum"] = defineFunction(3, sum)
    context["∏"] = context["product"] = defineFunction(3, product)

    return context

def test():
    context = createStdContext()
    print(Call(Variable("="), [Int(2), Int(3)]).evaluate(context))
    print(Call(Variable("<="), [Int(2), Int(3)]).evaluate(context))
