from lark               import *

from mi.interpreter.ast import *


parser = Lark(r"""
    INT      : /[0-9]+/
    DEC      : /[0-9]*[.][0-9]+/
    NAME     : /[a-zA-Z_][a-zA-Z_0-9]*/ | /[πτ⅀∏]/
    EQ       : "="
    NOT_EQ   : "/=" | "≠"
    _AND     : "and"
    _OR      : "or"
    _NOT     : "not"
    GREAT    : ">"
    LOW      : "<"
    GREAT_EQ : ">=" | "≥"
    LOW_EQ   : "<=" | "≤"
    PLUS     : "+"
    MINUS    : "-"
    MUL      : "*"
    DIV      : "/"
    MOD      : "%"
    EXPON    : "^"
    _POW2    : "²"
    _POW3    : "³"
    _POW4    : "⁴"
    _SQRT    : "√"
    _CBRT    : "∛"
    _TSRT    : "∜"
    IGNORED  : COMMENT | WHITE
    COMMENT  : "|" /[^;\n]/*
    WHITE    : (" " | "\n")+
    _LEFT    : "("
    _RIGHT   : ")"
    _COMMA   : ","
    _EXCLAM  : "!"
    _ARROW   : "->" | "→"
    _SEMI    : ";"
    _ASSIGN  : ":=" | "≝"
    _IF      : "if"
    _THEN    : "then"
    _ELSE    : "else"

    %ignore IGNORED


    ?start    : bind

    ?bind     : func | var_bind | func_bind
    var_bind  : NAME _assign
    func_bind : NAME paren_args _assign
    paren_args : _args
    _assign   : _ASSIGN start _SEMI start

    ?func     : args _ARROW func | cond
    args      : NAME
              | _args
    _args     : _LEFT NAME (_COMMA NAME)* _COMMA? _RIGHT

    ?cond     : _IF or _THEN start _ELSE start | or

    ?or       : and (_OR or)?
    ?and      : _not (_AND and)?
    _not      : not | rel
    not       : _NOT rel

    ?rel      : sum (rel_op rel)?
    ?rel_op   : EQ | NOT_EQ | GREAT | LOW | GREAT_EQ | LOW_EQ

    ?sum      : prod (sum_op sum)?
    ?sum_op   : PLUS | MINUS

    ?prod     : sign (prod_op prod)?
    ?prod_op  : MUL | DIV | MOD

    ?sign     : sign_op? _root
    ?sign_op  : PLUS | MINUS

    _root     : sqrt | cbrt | tsrt | exp
    sqrt      : _SQRT exp
    cbrt      : _CBRT exp
    tsrt      : _TSRT exp

    ?exp      : exp2 | exp3 | exp4 | expn
    exp2      : _fact _POW2
    exp3      : _fact _POW3
    exp4      : _fact _POW4
    ?expn     : _fact (EXPON exp)?

    _fact     : fact | call
    fact      : call _EXCLAM

    ?call     : callee _LEFT start (_COMMA start)* _COMMA? _RIGHT | val
    ?callee   : call | NAME | parens

    ?val      : INT | DEC | NAME | parens

    ?parens   : _LEFT start _RIGHT
""")

def expr(string):
    parsed = parser.parse(string)

    def convert(expr):
        # higher syntax
        if type(expr) is Tree:
            # binary
            if expr.data in ("rel", "sum", "prod", "expn"):
                [left, op, right] = expr.children
                return Call(
                    name = Variable(name = str(op)),
                    args = [convert(left), convert(right)]
                )
            # unicode exponents
            elif expr.data in ("exp2", "exp3", "exp4"):
                return Call(
                    name = Variable(name = "^"),
                    args = [convert(expr.children[0]), Int(value = int(expr.data[-1]))]
                )
            # unicode roots
            elif expr.data in ("sqrt", "cbrt", "tsrt"):
                return Call(
                    name = Variable(name = {"sqrt": "√", "cbrt": "∛", "tsrt": "∜"}[expr.data]),
                    args = [convert(expr.children[0])],
                )
            # prefix sign
            elif expr.data == "sign":
                [op, right] = expr.children
                if op.type == "PLUS":
                    return convert(right)
                elif op.type == "MINUS":
                    return Call(
                        name = Variable(name = "-"),
                        args = [Int(value = 0), convert(right)]
                    )
            # fact
            elif expr.data == "fact":
                return Call(
                    name = Variable(name = "!"),
                    args = [convert(expr.children[0])]
                )
            # function call
            elif expr.data == "call":
                callee, args = expr.children[0], expr.children[1:]
                return Call(
                    name = convert(callee),
                    args = [convert(arg) for arg in args],
                )
            # function declaration
            elif expr.data == "func":
                [args, body] = expr.children
                return Function(
                    args = [convert(arg) for arg in args.children],
                    body = convert(body),
                )
            # variable binding
            elif expr.data == "var_bind":
                [name, value, body] = expr.children
                return Call(
                    name = Function(
                        args = [convert(name)],
                        body = convert(body),
                    ),
                    args = [convert(value)],
                )
            # function binding
            elif expr.data == "func_bind":
                [name, args, value, body] = expr.children
                return Call(
                    name = Function(
                        args = [convert(name)],
                        body = convert(body),
                    ),
                    args = [Function(
                        args = [convert(arg) for arg in args.children],
                        body = convert(value)
                    )]
                )
            # conditionals
            elif expr.data == "cond":
                return Call(
                    name = Variable(name = "$if"),
                    args = [convert(arg) for arg in expr.children],
                )
            # logic operators
            elif expr.data in ("and", "or"):
                [left, right] = expr.children
                return Call(
                    name = Variable(name = f"${expr.data}"),
                    args = [convert(left), convert(right)],
                )
            # negation
            elif expr.data == "not":
                return Call(
                    name = Variable(name = "$not"),
                    args = [convert(expr.children[0])]
                )
            else:
                raise ValueError(f"'{tree}' is not supported")
        # literals
        elif type(expr) is Token:
            if expr.type == "INT":
                return Int(value = int(expr))
            elif expr.type == "DEC":
                return Float(value = float(expr))
            elif expr.type == "NAME":
                return Variable(name = str(expr))
            else:
                raise ValueError(f"'{tree}' is not supported")

    return convert(parsed)
